from django.test import TestCase,Client
from django.urls import reverse
from selenium import webdriver

# Create your tests here.
class test(TestCase):
    def test_landing_page(self):
        response = Client().get("")
        self.assertEqual(response.status_code, 200)

    def test_landing_page_template(self):
        response = Client().get("")
        self.assertTemplateUsed(response, 'story7.html')