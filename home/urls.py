from django.urls import path
from .views import index


appname = 'home'

urlpatterns = [
   path('', index, name = 'accordion'),
]
